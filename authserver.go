package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/mail"
	"regexp"
	"strings"
	"time"

	"github.com/gorilla/sessions"
	"gitlab.com/krobolt/go-dispatcher"
	skeleton "gitlab.com/krobolt/go-skeleton"
	auth "gitlab.com/krobolt/go-skeleton/crypto"
	"gitlab.com/krobolt/go-storage"
)

//AuthServer handlers for account registration, activation, recovery, deactivation
//Sessions stored in reddis
//TODO: encrypt data
type AuthServer struct {
	database *storage.DBC
	*session
}

type session struct {
	redis      *storage.RedisCache
	cookies    *sessions.CookieStore
	expires    time.Duration
	cookiename string
}

type cookiedata struct {
	Session string `json:"session"`
	IP      string `json:"ip"`
}

func NewAuthServer(db *storage.DBC, redis *storage.RedisCache, secret []byte, cookiename string, expires time.Duration) *AuthServer {
	return &AuthServer{
		db,
		&session{
			redis,
			sessions.NewCookieStore(secret),
			expires,
			cookiename,
		},
	}
}

func (as *AuthServer) Dispatcher(baseURL string, d dispatcher.Dispatcher) dispatcher.Dispatcher {
	d.Group(baseURL, func() {
		d.Add("POST", "/register", as.SubmitRegisterAccount, nil)
		d.Add("POST", "/activate", as.SubmitActivateAccount, nil)
		d.Add("POST", "/login", as.SubmitLogin, nil)
		d.Add("POST", "/logout", as.SubmitLogoutAccount, nil)
		d.Add("POST", "/recover", as.SubmitRecover, nil)
		d.Add("POST", "/deactivate", as.SubmitDeactivateAccount, nil)
	}, nil)
	return d
}

//SubmitRegisterAccount registers new account
//Sends activation code to email address provided via SMTP
func (as *AuthServer) SubmitRegisterAccount(w http.ResponseWriter, r *http.Request) {
	var (
		path     = html.EscapeString(r.URL.Path)
		response = skeleton.NewResponse()
	)

	response.Code = http.StatusBadRequest
	response.Error = "unknown error, please try again"

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		response.Code = http.StatusInternalServerError
		response.Respond(w)
	}()

	if r.Method != http.MethodPost {
		response.Error = "method not allowed, only POST supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	inputs, err := skeleton.GetInputs(r, "username", "email", "password")
	if err != nil {
		response.Error = err.Error()
		response.Code = http.StatusUnprocessableEntity
		return
	}

	email, err := mail.ParseAddress(inputs["email"])

	if err != nil {
		response.Error = "email address is not invalid"
		return
	}

	err = validateNewPassword(inputs["password"])
	if err != nil {
		response.Error = err.Error()
		return
	}

	passwordHash, err := auth.HashPassword(inputs["password"], 15)
	if err != nil {
		response.Error = "unable to creeate password"
		return
	}

	usermodel := NewUserModel(storage.NewModel(as.database, as.redis))
	usermodel.LeaveOpen()
	defer usermodel.Close()

	ok, _ := usermodel.QueryUsernameUnique(inputs["username"])
	if !ok {
		response.Error = "username already taken"
		return
	}

	ok, _ = usermodel.QueryEmailUnique(email.Address)
	if !ok {
		response.Error = "email already taken"
		return
	}

	userid, err := auth.HashPassword(inputs["username"], 15)
	if err != nil {
		response.Code = http.StatusUnprocessableEntity
		return
	}

	activationtoken := generateKey(64, 16)
	recoverycode := generateKey(64, 16)

	err = usermodel.Insert(
		string(userid),
		inputs["username"],
		email.Address,
		string(passwordHash),
		activationtoken,
		recoverycode,
		11, //TODO: add roles
	)

	if err != nil {
		response.Code = http.StatusUnprocessableEntity
		log.Println(err)
		return
	}

	//todo: send activation email
	//err = skeleton.NewActivationEmail(email.Address, activationcode)
	//if err != nil {
	//	response.Error = "unable to send activation email, please try later"
	/*
		//remove record from db as we where unable to send activation email.
		ok, err := usermodel.Remove(userid)
		if !ok || err != nil {
			//unable to remove record
			log.Println("unable to remove redundant record. id:", userid)
		}
		return
	*/
	//}

	response.Code = http.StatusCreated
	response.Data = activationtoken
	response.Error = activationtoken
	return
}

//SubmitLogin login to account
func (as *AuthServer) SubmitLogin(w http.ResponseWriter, r *http.Request) {
	var (
		path           = html.EscapeString(r.URL.Path)
		response       = skeleton.NewResponse()
		rejectErrorMsg = "username/password is incorrect"
	)

	response.Code = http.StatusAccepted
	response.Error = "unknown error, please try again"

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		response.Respond(w)
	}()

	if r.Method != http.MethodPost {
		response.Error = "method not allowed, only POST supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	response.Error = rejectErrorMsg

	inputs, err := skeleton.GetInputs(r, "username", "password")
	if err != nil {
		log.Println(err)
		return
	}
	usermodel := NewUserModel(storage.NewModel(as.database, as.redis))
	usermodel.LeaveOpen()
	defer usermodel.Close()

	_, err = as.processLogin(w, r, usermodel, inputs["username"], inputs["password"])

	if err != nil {
		log.Println(err)
		return
	}

	response.Code = http.StatusAccepted
	response.Data = "account logged in"
	response.Error = "login successful"
	return
}

//SubmitActivateAccount Process activation token
//Json GET endpoint, returning http status and error message
func (as *AuthServer) SubmitActivateAccount(w http.ResponseWriter, r *http.Request) {

	path := html.EscapeString(r.URL.Path)
	response := skeleton.NewResponse()
	response.Error = "invalid activiation code"

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		response.Respond(w)
	}()

	if r.Method != http.MethodPost {
		response.Error = "method not allowed"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	inputs, err := skeleton.GetInputs(r, "username", "password", "token")
	if err != nil {
		return
	}

	usermodel := NewUserModel(storage.NewModel(as.database, as.redis))
	usermodel.LeaveOpen()

	defer usermodel.Close()

	user, err := as.processLogin(w, r, usermodel, inputs["username"], inputs["password"])
	if err != nil {
		return
	}

	code, err := usermodel.GetActivation(user.ID)

	if inputs["token"] != code {
		return
	}

	ok, err := usermodel.Activate(user.ID)

	if !ok {
		return
	}

	response.Code = http.StatusAccepted
	response.Error = ""
	response.Data = "account activated"
}

//SubmitDeactivateAccount makes account inactive but recoverable
//TODO: datestamp account so it can be removed after X time from database
func (as *AuthServer) SubmitDeactivateAccount(w http.ResponseWriter, r *http.Request) {

	path := html.EscapeString(r.URL.Path)
	response := skeleton.NewResponse()

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		response.Respond(w)
	}()

	if r.Method != http.MethodPost {
		response.Error = "method not allowed"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	s, err := as.cookies.Get(r, as.cookiename)
	if err != nil {
		//TODO handle session cookie error
		log.Println(s, err)
		return
	}

	cookieuser, err := as.getSessionFromCookie(s.Values)
	if err != nil {
		log.Println(err)
		delete(s.Values, as.cookiename)
		s.Save(r, w)
		return
	}

	sessionid, ok := as.redis.Get(cookieuser.Session)
	if !ok {
		return
	}

	sessionUser := &User{}
	decoder := json.NewDecoder(strings.NewReader(sessionid))
	err = decoder.Decode(sessionUser)

	usermodel := NewUserModel(storage.NewModel(as.database, as.redis))

	newpassword, err := auth.HashPassword(generateKey(64, 32), 15)
	if err != nil {
		log.Panicln(err)
		return
	}

	ok, err = usermodel.Deactivate(sessionUser.ID, string(newpassword))
	if !ok {
		log.Println(err)
		return
	}

	//logout
	as.redis.Client.Del(sessionUser.Session)

	response.Code = http.StatusAccepted
	response.Error = ""
}

//SubmitLogoutAccount active account
func (as *AuthServer) SubmitLogoutAccount(w http.ResponseWriter, r *http.Request) {

	path := html.EscapeString(r.URL.Path)
	response := skeleton.NewResponse()

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		response.Respond(w)
	}()

	if r.Method != http.MethodPost {
		response.Error = "method not allowed"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	s, err := as.cookies.Get(r, as.cookiename)
	if err != nil {
		//TODO handle session cookie error
		log.Println(s, err)
		return
	}

	cookieuser, err := as.getSessionFromCookie(s.Values)
	if err != nil {
		log.Println(err)
		delete(s.Values, as.cookiename)
		s.Save(r, w)
		return
	}

	sessionid, ok := as.redis.Get(cookieuser.Session)
	if !ok {
		return
	}

	sessionUser := &User{}
	decoder := json.NewDecoder(strings.NewReader(sessionid))
	err = decoder.Decode(sessionUser)

	//TODO: compare session information with cookie before deleting,
	//add username/email to cookie for comparing

	//logout by removing session/cookie
	as.redis.Client.Del(sessionid)
	delete(s.Values, as.cookiename)
	s.Save(r, w)

	response.Code = http.StatusAccepted
	response.Error = ""
}

//SubmitRecover send email to user to reactivate
func (as *AuthServer) SubmitRecover(w http.ResponseWriter, r *http.Request) {
	var (
		path     = html.EscapeString(r.URL.Path)
		response = skeleton.NewResponse()
	)
	response.Code = http.StatusInternalServerError
	response.Error = "unknown error, please try again"

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		response.Respond(w)
	}()

	if r.Method != http.MethodPost {
		response.Error = "method not allowed, only POST supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	input, err := skeleton.GetInputs(r, "username")

	usermodel := NewUserModel(storage.NewModel(as.database, as.redis))

	user, err := usermodel.GetRecovery(input["username"])
	if err != nil {
		//invalid username provided
		return
	}

	//TODO: send email to user.Email
	log.Println(user)
	//recover/activation code
	code := generateKey(64, 16)

	if !user.IsActive {
		//set activation code
		//send new code to email
		return
	}

	ok, err := usermodel.RecoverAccount(code, input["username"])
	if !ok {
		log.Println(err)
		return
	}

	//send email to account
	log.Printf("send activation email to %s with code %s \n", user.Email, code)

	response.Code = http.StatusAccepted
	response.Data = "recovery email sent"
	response.Error = ""
	return
}

func (as *AuthServer) getSessionFromCookie(v map[interface{}]interface{}) (*cookiedata, error) {
	cookieuser := &cookiedata{
		Session: "",
		IP:      "",
	}

	if v[as.cookiename] != nil {
		switch v[as.cookiename].(type) {
		case string:
			cookiestring := v[as.cookiename].(string)
			decoder := json.NewDecoder(strings.NewReader(cookiestring))
			return cookieuser, decoder.Decode(cookieuser)

		default:
			return cookieuser, errors.New("invalid session cookie")
		}
	}
	return cookieuser, nil
}

func (as *AuthServer) processLogin(w http.ResponseWriter, r *http.Request, usermodel *UserModel, username, password string) (*User, error) {

	user, err := usermodel.FetchLogin(username)

	cookie := &cookiedata{
		Session: "",
		IP:      "",
	}

	if err != nil {
		log.Println(err)
		return &User{}, err
	}

	if !auth.VerifyPassword(password, user.Hash) {
		log.Println("failed log in attempt: ", username)
		return &User{}, errors.New("failed to verify password")
	}

	uniqueSession := func(id string) (string, error) {
		for i := 0; i < 10; i++ {
			_, ok := as.redis.Get(user.Session)
			if !ok {
				return id, nil
			}
			id = generateKey(32, 32)
		}
		return id, fmt.Errorf("unable to create unique session id")
	}

	user.Session, err = uniqueSession(generateKey(32, 32))
	if err != nil {
		log.Panicln(err)
		return &User{}, err
	}

	cookie.Session = user.Session

	user.IP, err = getIP(r)
	if err != nil {
		return &User{}, fmt.Errorf("invalid ip %s", err)
	}

	cookie.IP = user.IP

	s, err := as.cookies.Get(r, as.cookiename)
	if err != nil {
		log.Println("unable to find cookie", err)
		return &User{}, err
	}

	suser, err := json.Marshal(user)
	if err != nil {
		log.Println(err)
		return &User{}, err
	}

	ok, err := as.redis.Set(user.Session, suser, as.expires)
	if !ok {
		log.Println(err)
		return &User{}, err
	}

	cookiedata, err := json.Marshal(cookie)
	if err != nil {
		log.Println(err)
		return &User{}, err
	}
	s.Values[as.cookiename] = string(cookiedata)
	err = s.Save(r, w)
	return user.User, err
}

//Firewall displatcher middleware
func (as *AuthServer) Firewall(role interface{}, redirect string) dispatcher.Middleware {
	// Create a new Middleware
	return func(f http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			defer func() {
				log.Println(time.Since(start))
			}()

			sessionUser := &User{}

			s, err := as.cookies.Get(r, as.cookiename)
			if err != nil {
				//TODO handle session cookie error
				log.Println(s, err)
				http.Redirect(w, r, redirect, http.StatusTemporaryRedirect)
				return
			}

			cookieuser, err := as.getSessionFromCookie(s.Values)
			if err != nil {
				log.Println(err)
				delete(s.Values, as.cookiename)
				s.Save(r, w)
				http.Redirect(w, r, redirect, http.StatusTemporaryRedirect)
				return
			}

			c, ok := as.redis.Get(cookieuser.Session)

			if !ok {
				log.Println("not logged in, reddirect response")
				http.Redirect(w, r, redirect, http.StatusTemporaryRedirect)
				return
			}

			decoder := json.NewDecoder(strings.NewReader(c))
			err = decoder.Decode(sessionUser)

			if err != nil {
				log.Println(err)
			}

			requestIP, err := getIP(r)
			if requestIP != cookieuser.IP {
				log.Printf("IP %s does not match that of cookies: %s \n", requestIP, cookieuser.IP)
				//http.Redirect(w, r, redirect, http.StatusTemporaryRedirect)
			}

			if sessionUser.IsActive == false {
				log.Println("inactive:,", sessionUser.IsActive, sessionUser, cookieuser)
				http.Redirect(w, r, redirect, http.StatusTemporaryRedirect)
				return
			}

			switch role.(type) {
			case int:
				if sessionUser.Role < role.(int) {
					log.Println("wrong level to access content", sessionUser, role)
					http.Redirect(w, r, redirect, http.StatusPermanentRedirect)
					return
				}
			case string:
				if sessionUser.NamedRole == role.(string) {
					log.Println("wrong level to access content", sessionUser, role)
					http.Redirect(w, r, redirect, http.StatusPermanentRedirect)
					return
				}

			default:
				http.Redirect(w, r, redirect, http.StatusPermanentRedirect)
				return
			}

			sessionUser.Session = generateKey(32, 32)
			_, isFound := as.redis.Get(cookieuser.Session)
			if isFound {
				log.Println("removing old session", cookieuser.Session, sessionUser.Session)
				as.redis.Client.Del(cookieuser.Session)
			}

			suser, err := json.Marshal(sessionUser)
			if err != nil {
				log.Println(err)
				return
			}

			//TODO: check that id doesn't aleady exist
			ok, err = as.redis.Set(sessionUser.Session, suser, as.expires)
			if !ok {
				log.Println(err)
				return
			}

			cookieuser.Session = sessionUser.Session

			cookiedata, err := json.Marshal(cookieuser)
			if err != nil {
				log.Println(err)
				return
			}

			s.Values[as.cookiename] = string(cookiedata)
			s.Save(r, w)

			// Call the next middleware/handler in chain
			f(w, r)
		}
	}
}

func generateKey(base, n int) string {
	r := []string{
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "e", "f",
		"g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
		"v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
		"Z", "_", "-", ".",
	}
	out := make([]string, n)
	for i := 0; i < n; i++ {
		out = append(out, r[rand.Intn(base)])
	}
	return strings.Join(out, "")
}

func getIP(r *http.Request) (string, error) {

	var netP net.IP

	ip := r.Header.Get("X-FORWARDED-FOR")
	ips := strings.Split(ip, ",")
	for _, ip := range ips {
		netP = net.ParseIP(ip)
		if netP != nil {
			return ip, nil
		}
	}

	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return "", err
	}
	netP = net.ParseIP(ip)
	if netP != nil {
		return ip, nil
	}
	return "", errors.New("No valid ip found")
}

func validateNewPassword(password string) error {

	m := regexp.MustCompile(`[^A-Za-z0-9]`)
	re := m.FindStringSubmatch(password)
	if len(re) == 0 {
		return errors.New("password must have at least 1 special character")
	}
	m = regexp.MustCompile(`(.*[A-Z].*)`)
	re = m.FindStringSubmatch(password)
	if len(re) == 0 {
		return errors.New("password must have at least one uppercase letter")
	}
	m = regexp.MustCompile(`(.*[a-z].*)`)
	re = m.FindStringSubmatch(password)
	if len(re) == 0 {
		return errors.New("password must have at least one lowercase letter")
	}
	m = regexp.MustCompile(`(.*\d.*)`)
	re = m.FindStringSubmatch(password)
	if len(re) == 0 {
		return errors.New("password must have at least one digit")
	}
	return nil
}
