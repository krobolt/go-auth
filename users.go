package main

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"errors"
	"log"
	"strings"
	"time"

	storage "gitlab.com/krobolt/go-storage"
)

type UserModel struct {
	*storage.Model
	expire time.Duration
	trys   int
}

//User data publically available information for user
type User struct {
	ID        string `json:"id"`
	Username  string `json:"username"`
	Email     string `json:"email"`
	Role      int    `json:"role"`
	NamedRole string `json:"-"` //not used as of yet
	Session   string `json:"session"`
	IP        string `json:"ip"`
	IsActive  bool   `json:"active"`
}

//PrivateUser private data for user
type PrivateUser struct {
	*User
	FirstName string `json:"-"`
	LastName  string `json:"-"`
	Hash      string `json:"hash"`
}

const (
	sqlUsersUp = `
	CREATE TABLE USER_TBL(
	Userid varchar(64) NOT NULL UNIQUE,
	Username varchar(49) NOT NULL UNIQUE,	
	Email varchar(90) NOT NULL UNIQUE,	
	Hash varchar(150) NOT NULL,	
	ActivationCode varchar(150) NOT NULL,	
	RecoveryCode varchar(150) NOT NULL, 
	Is_Active bool,
	Role int,
	PRIMARY KEY (Userid)
	); 
`

	sqlUserInsert           = `INSERT INTO USER_TBL (Userid, Username, Email, Hash, ActivationCode, RecoveryCode, Is_Active, Role) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`
	sqlUserVerifyActivation = `SELECT ActivationCode from USER_TBL WHERE Userid = ? and Is_Active=0`

	//user activation and deactivation
	//activate user by submiting confirmation email.
	//deactivate account when suspecious activity is detected or in the event of a delete/account removal
	sqlUserActivate   = `UPDATE USER_TBL SET Is_Active=1 WHERE Userid = ?;`
	sqlUserDeactivate = `UPDATE USER_TBL SET Hash=? Is_Active=0 WHERE Userid = ?;`

	sqlLoginUsername   = `SELECT Userid, Username, Email, Hash, Role, Is_Active from USER_TBL WHERE Username = ?`
	sqlRecoverUsername = `Email, Is_Active FROM USER_TBL WHERE Username = ?`
	sqlUserDetails     = `SELECT Userid, Last_Name, First_Name from USER_TBL WHERE Userid = ?`

	//unique checks
	sqlUsernameUnique = `SELECT Username from USER_TBL WHERE Username = ? LIMIT 1`
	sqlEmailUnique    = `SELECT Email from USER_TBL WHERE Email = ? LIMIT 1;`

	sqlUserUpdateActivation = `"UPDATE USER_TBL SET RecoveryCode=? WHERE Email=?;"`
	sqlUserUpdatePassword   = `"UPDATE Hash=? WHERE Userid=?"`

	//sqlUserDelete = `DELETE FROM USER_TABLE WHERE Userid=?;`
)

func NewUserModel(m *storage.Model) *UserModel {
	return &UserModel{m, time.Minute * 3, 1}
}

func NewPrivateUser() *PrivateUser {
	return &PrivateUser{
		&User{
			"defaultid",
			"default_username",
			"default_email",
			0,
			"",
			"",
			"",
			false,
		},
		"default_firstname",
		"default_secondname",
		"defaulthash",
	}
}

//Up execute create user model table
func (user *UserModel) Up() (bool, error) {
	db, err := user.DB.Open()
	defer db.Close()
	if err != nil {
		return false, err
	}
	stmt, err := db.Prepare(sqlUsersUp)
	defer stmt.Close()
	res, err := stmt.Exec()
	if err != nil {
		return false, err
	}
	_, err = res.LastInsertId()
	if err != nil {
		return false, err
	}
	return true, err
}

//Insert new user acccount
func (user *UserModel) Insert(
	id,
	username,
	email,
	hash,
	activationcode,
	recovery string,
	role int,
) (err error) {

	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return err
		}
		db = dbc
		defer db.Close()
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	defer rollback(err, tx)

	stmt, err := tx.Prepare(sqlUserInsert)
	defer stmt.Close()

	_, err = stmt.Exec(
		id,
		username,
		email,
		hash,
		activationcode,
		recovery,
		0,
		role,
	)
	return err
}

func rollback(err error, tx *sql.Tx) error {
	switch err {
	case nil:
		err = tx.Commit()
	default:
		tx.Rollback()
	}
	if err != nil {
		return err
	}
	return err
}

//GetRecovery information for user account
func (user *UserModel) GetRecovery(username string) (*PrivateUser, error) {
	dbuser := NewPrivateUser()
	dbuser.Username = username
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return dbuser, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlRecoverUsername)
	defer stmt.Close()
	rows, err := stmt.Query(username)
	defer rows.Close()
	if err != nil {
		return dbuser, err
	}
	for rows.Next() {
		err = rows.Scan(&dbuser.Email, &dbuser.IsActive)
		if err != nil {
			return dbuser, err
		}
	}
	return dbuser, nil
}

//RecoverAccount set new recovery id
func (user *UserModel) RecoverAccount(activationcode, email string) (bool, error) {
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}

	//update the activation code with new code
	stmt, err := db.Prepare(sqlUserUpdateActivation)
	defer stmt.Close()
	_, err = stmt.Exec(activationcode, email)
	if err != nil {
		return false, err
	}
	return true, nil
}

//Activate user account
func (user *UserModel) Activate(id string) (bool, error) {
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}

	stmt, err := db.Prepare(sqlUserActivate)

	log.Println(sqlUserActivate)
	log.Println(id)

	defer stmt.Close()

	_, err = stmt.Exec(string(id))
	if err != nil {
		return false, err
	}
	return true, nil
}

//Deactivate user account
func (user *UserModel) Deactivate(id, password string) (bool, error) {
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlUserDeactivate)
	defer stmt.Close()
	_, err = stmt.Exec(id)
	if err != nil {
		return false, err
	}
	return true, nil
}

//GetActivation information for user account
func (user *UserModel) GetActivation(id string) (string, error) {
	var dbsecret string
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return dbsecret, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlUserVerifyActivation)
	defer stmt.Close()
	rows, err := stmt.Query(id)
	defer rows.Close()
	if err != nil {
		return dbsecret, err
	}
	for rows.Next() {
		err = rows.Scan(&dbsecret)
		if err != nil {
			return dbsecret, err
		}
	}

	return dbsecret, err
}

//FetchUser returns sensistive user infomration from database
//not saved to cache
func (user *UserModel) FetchUser(username string) (*PrivateUser, error) {
	return user.queryUser(username)
}

//FetchLogin retrives login information from database
//checks cache storage first
//saves to cache if result found
func (user *UserModel) FetchLogin(username string) (*PrivateUser, error) {

	hash := md5.Sum([]byte(username))
	cacheid := hex.EncodeToString(hash[:])

	dbuser := NewPrivateUser()
	ok := user.fetchCache(cacheid, dbuser)
	if ok {
		user.trys++
		return dbuser, nil
	}

	//no cache found, first attempt
	dbuser, err := user.queryLogin(username)
	ok = user.saveCache(cacheid, dbuser)
	if !ok {
		return dbuser, errors.New("unable to cache login information")
	}
	return dbuser, err
}

//QueryUsernameUnique check if username already in database
func (user *UserModel) QueryUsernameUnique(username string) (bool, error) {
	return user.queryUnique(username, sqlUsernameUnique)
}

//QueryEmailUnique check if email already in database
func (user *UserModel) QueryEmailUnique(email string) (bool, error) {
	return user.queryUnique(email, sqlEmailUnique)
}

func (user *UserModel) queryUnique(input, sql string) (bool, error) {
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sql)
	defer stmt.Close()
	rows, err := stmt.Query(input)
	defer rows.Close()
	if err != nil {
		return false, err
	}
	result := ""

	for rows.Next() {
		err = rows.Scan(&result)
		if err != nil {
			return false, err
		}
	}
	if result != "" {
		return false, nil
	}
	return true, nil
}

func (user *UserModel) queryUser(username string) (*PrivateUser, error) {
	dbuser := NewPrivateUser()
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return dbuser, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlLoginUsername)
	defer stmt.Close()
	rows, err := stmt.Query(username)
	defer rows.Close()
	if err != nil {
		return dbuser, err
	}
	for rows.Next() {
		err = rows.Scan(&dbuser.Username, &dbuser.Email, &dbuser.Hash)
		if err != nil {
			return dbuser, err
		}
	}
	return dbuser, nil
}

func (user *UserModel) queryLogin(username string) (*PrivateUser, error) {
	dbuser := NewPrivateUser()
	db := user.Conn
	if !user.Open {
		dbc, err := user.DB.Open()
		if err != nil {
			return dbuser, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlLoginUsername)
	defer stmt.Close()
	rows, err := stmt.Query(username)
	defer rows.Close()
	if err != nil {
		return dbuser, err
	}
	for rows.Next() {
		err = rows.Scan(&dbuser.ID, &dbuser.Username, &dbuser.Email, &dbuser.Hash, &dbuser.Role, &dbuser.IsActive)
		if err != nil {
			return dbuser, err
		}
	}
	return dbuser, nil
}

func (user *UserModel) fetchCache(id string, dst *PrivateUser) bool {
	result, ok := user.Cache.Get(id)
	if ok {
		decoder := json.NewDecoder(strings.NewReader(result))
		err := decoder.Decode(dst)
		if err != nil {
			return false
		}
		return true
	}
	return false
}

func (user *UserModel) saveCache(id string, source *PrivateUser) bool {
	b, err := json.Marshal(source)
	if err != nil {
		return false
	}
	ok, err := user.Cache.Set(id, string(b), user.expire)
	if !ok || err != nil {
		return false
	}
	return true
}
